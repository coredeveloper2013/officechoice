<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemSupplier;
use App\Object20;
use App\Object65;
use App\Object66;
use App\Object72;
use App\Supplier;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{

    public function home(Request $request)
    {

        $suppliers = Supplier::orderBy('supplierName')->get();
        $last_search = session('last_search');

        return view('products.home', compact('suppliers', 'last_search'));

    }

    public function filter(Request $request)
    {


        $input['searchstring'] = request('search') ? request('search') : '';
        $input['supplier'] = request('supplier') ? request('supplier') : '';
        $input['allwords'] = request('allwords') ? request('allwords') : '';
        $input['allsupplier'] = request('allsupplier') ? request('allsupplier') : '';
        $input['oconly'] = request('oconly') ? request('oconly') : '';
        $input['discontinued'] = request('discontinued') ? request('discontinued') : '';
        $input['currentcatalog'] = request('currentcatalog') ? request('currentcatalog') : '';
        $input['limit'] = request('limit') ? request('limit') : 10;
        $input['sorting'] = request('sorting') ? request('sorting') : '';
        $input['sortBY'] = '';
        $input['sort_order'] = '';

        session(['last_search' => $input['searchstring']]);


        if ($input['sorting'] != '') {
            $explode = explode(' ', $input['sorting']);

            if (strpos($explode[0], 'its.') === 0) {
                $input['sortBY'] = str_replace("its.", "ocl_itemsupplier.", $explode[0]);// take here table attribute
                $input['sort_order'] = $explode[1]; // take here order type asc or desc

            } elseif (strpos($explode[0], 'it.') === 0) {
                $input['sortBY'] = str_replace("it.", "ocl_item.", $explode[0]); // take here table attribute
                $input['sort_order'] = $explode[1];//// take here order type asc or desc
            }
        }


        $sql = ItemSupplier::select('ocl_itemsupplier.itemCode', 'ocl_itemsupplier.itemSupplierCode', 'ocl_itemsupplier.itemCost1', 'ocl_itemsupplier.buyUom', 'ocl_itemsupplier.supplierCode', 'ocl_itemsupplier.fcDate', 'ocl_itemsupplier.futureCost', 'ocl_item.objectId', 'ocl_item.internetItemName', 'ocl_item.dateItemActive', 'ocl_item.recordModifiedDate', 'ocl_item.productStatus')
            ->leftJoin('ocl_item', 'ocl_item.itemCode', 'ocl_itemsupplier.itemCode')
            ->whereNotNull('ocl_item.itemCode')
            ->where(function ($q) use ($input) {

                if ($input['allwords'] != '') {
                    if ($input['searchstring'] != "") {
                        $searchstring = strtolower($input['searchstring']);

//                        $words = explode(' ', trim(preg_replace('/\s+/', ' ', $searchstring)));
                        $word = $searchstring;

//                        foreach ($words as $word) {

                            $q->where('ocl_item.internetItemName', 'Like', '%' . $word . '%');
                            $q->orWhere('ocl_item.legacyItemCode', 'Like', '%' . $word . '%');
                            $q->orWhere('ocl_item.replacedProduct', 'Like', '%' . $word . '%');
                            $q->orWhere('ocl_item.internetDescription', 'Like', '%' . $word . '%');
                            $q->orWhere('ocl_item.itemCode', 'Like', '%' . $word . '%');
                            $q->orWhere('ocl_item.objectIdText', 'Like', '%' . $word . '%');
                            $q->orWhere('ocl_itemsupplier.itemSupplierCode', 'Like', '%' . $word . '%');
                            $q->orWhere('ocl_item.barcodeSellUOM', 'Like', '%' . $word . '%');
//                        }
                    }
                } else {
                    if ($input['searchstring'] != "") {
                        $searchstring = strtolower($input['searchstring']);

                        $q->where('ocl_item.internetItemName', 'Like', '%' . $searchstring . '%');
                        $q->orWhere('ocl_item.legacyItemCode', 'Like', '%' . $searchstring . '%');
                        $q->orWhere('ocl_item.replacedProduct', 'Like', '%' . $searchstring . '%');
                        $q->orWhere('ocl_item.internetDescription', 'Like', '%' . $searchstring . '%');
                        $q->orWhere('ocl_item.itemCode', 'Like', '%' . $searchstring . '%');
                        $q->orWhere('ocl_item.objectIdText', 'Like', '%' . $searchstring . '%');
                        $q->orWhere('ocl_itemsupplier.itemSupplierCode', 'Like', '%' . $searchstring . '%');
                        $q->orWhere('ocl_item.barcodeSellUOM', 'Like', '%' . $searchstring . '%');
                    }
                }
            });

        if ($input['supplier'] != 'all') {
            $sql->where('ocl_itemsupplier.supplierCode', '=', $input['supplier']);
        }

        if ($input['oconly'] != '') {
            $sql->where('ocl_item.availability', '=', 'oconly');
        }

        if ($input['discontinued'] == '') {
            $sql->where('ocl_item.productStatus', '=', 'active');
            $sql->where('ocl_itemsupplier.productStatus', '=', 'active');
        }

        if ($input['currentcatalog'] != '') {
            $sql->where('ocl_item.isFLCFlag', '=', 'yes');
        }

        if ($input['allsupplier'] != '') {
//            $sql->groupBy('ocl_itemsupplier.itemCode');

            if ($input['sorting'] != '') {
                $sql->orderBy($input['sortBY'], $input['sort_order']);
            }

            $sql->orderBy('ocl_itemsupplier.supplierItemPriority', 'ASC');
        } else {
            $sql->orderBy($input['sortBY'], $input['sort_order']);
            $sql->groupBy('ocl_itemsupplier.itemCode');
        }

        $products = $sql->paginate($input['limit']);


        if (count($products) < 1) {
            $json['items'] = '<div class="row"><div class="col-md-12">There are no products that match your search criteria. Please review your search options and try again.</div></div>';
            $json['total'] = 0;
            $json['totpag'] = "of 0";
            $json['pages'] = '';
            $json['arprev'] = '';
            $json['arnext'] = 'filter';
        } else {

            $thirtydaysless = date("Y-m-d h:i:s", strtotime(" -30 days"));
            $today = date("Y-m-d h:i:s");

            $page = request('page') ? request('page') : 1;
            $in_array = $products->toArray();

            $json['total'] = $products->total();
            $json['totpag'] = "of " . $in_array['last_page'];

            if ($in_array['current_page'] > 1)
                $json['arprev'] = '<img id="arprev" class="arprev" src="' . asset('image/arrow-left.png') . '" />';
            else
                $json['arprev'] = '';


            if ($in_array['next_page_url'] != null)
                $json['arnext'] = '<img id="arnext" class="arnext" src="' . asset('image/arrow-right.png') . '" />';
            else
                $json['arnext'] = '';

            $pagestring = '';
            for ($i = 1; $i <= $in_array['last_page']; $i++) {
                if ($page == $i) {
                    $pagestring .= '<option selected="selected" value="' . $i . '">' . $i . '</option>';
                } else {
                    $pagestring .= '<option value="' . $i . '">' . $i . '</option> ';
                }


            }

            $json['pages'] = $pagestring;

            $json['items'] = '';

            foreach ($in_array['data'] as $row) {
                $itemcode = $row['itemCode'];
                $objectId = $row['objectId'];
                $supplieritem = $row['itemSupplierCode'];
                $cost = $row['itemCost1'];
                $uom = $row['buyUom'];
                $itemdescription = $row['internetItemName'];
                if (!$itemdescription) {
                    $itemdescription = "No description saved in DB";
                }
                $codesupplier = $row['supplierCode'];

                $dateItemActive = date("Y-m-d h:i:s", (int)$row['dateItemActive']);
                $recordModifiedDate = date("Y-m-d h:i:s", (int)$row['recordModifiedDate']);
                $dbdataStatus = $row['productStatus'];
                $fcDate = date("Y-m-d h:i:s", $row['fcDate']);
                $futureCost = $row['futureCost'];

                if (file_exists(public_path()."/image/imageItemServer/" . $itemcode . "/" . $itemcode . ".jpg")) {
                    $pimage = asset('image/imageItemServer/' . $itemcode . '/' . $itemcode . '.jpg');
                } else {
                    $pimage = asset('image/imageItemServer/NoImage.jpg');
//                    $pimage = asset('image/imageItemServer/' . $itemcode . '/' . $itemcode . '.jpg');
//                    $pimage = asset('image/imageItemServer/ACO-1001001/ACO-1001001.jpg');
                }

                $statusimage = "status_none.png";
                $statusalt = "";

                if ($dateItemActive > $thirtydaysless) {
                    $statusimage = "status_new.png";
                    $statusalt = "New item";
                }

                if ($recordModifiedDate > $thirtydaysless) {
                    $statusimage = "status_mod.png";
                    $statusalt = "Modified Item";
                }

                if ($dbdataStatus == "inactive") {
                    $statusimage = "status_disc.png";
                    $statusalt = "Discontinued Item";
                }

                if (($fcDate > $today) && ($futureCost > $cost)) {
                    $statusimage = "status_pinc.png";
                    $statusalt = "Price Increase";
                }

                if (($fcDate > $today) && ($futureCost < $cost)) {
                    $statusimage = "status_pdec.png";
                    $statusalt = "Price Decrease";
                }


                $cost = sprintf("%.2f", $cost);
                //a href $supplieritem

                $json['items'] .= '
			<a href= "' . route('product.details', ['code' => $objectId]) . '" >
			<div class="row">

			<div class="col-md-1 imghov ">
			<img id= "' . $itemcode . '" src="' . $pimage . '" class="searchhoverimage"  />
			<div class="showhover">
			<img  id= "' . $itemcode . '" src="' . $pimage . '" class="showhoverimg"  />
			</div>
			</div>
			<div class="col-md-2">
			' . $itemcode . '
			</div>
			<div class="col-md-4">
			' . $itemdescription . '
			</div>
			<div class="col-md-1">
			' . $uom . '
			</div>
			<div class="col-md-2">
			' . $codesupplier . ' &nbsp ' . $supplieritem . '
			</div>
			<div class="col-md-1">

			' . $cost . '
			</div>
			<div class="col-md-1">
			<img class="status_product" alt="' . $statusalt . '" title="' . $statusalt . '" src="' . asset('image/' . $statusimage) . '" style="width:30px"/>
			</div>


			</div></a>
			';
            }


        }

        return $json;
    }

    public function csv_download($file)
    {
        if (file_exists(asset("oclimport/" . $file))) {
            return Response::download($file);

        } else {

            return 'no-file';
        }
    }

    public function details($code)
    {
        $sprefercost = null;
        $gst = 0;
        $num_cprows = 0;
        $cpresult = null;
        $promresult = null;
        $num_promrows = 0;
        $rpresult = null;
        $num_rprows = 0;
        $promoPrice = 0;
        $promoId = '';
        $promoCode = '';
        $promoType = '';
        $pritemresult = null;
        $prsupresult = null;
        $presult = null;
        $num_prsuprows = 0;
        $num_pritemrows = 0;
        $promoCost = '';
        $promoCostDiv = '';
        $promoSupplier = '';
        $promoSupplierName = '';
        $image = asset('image/NoImage.jpg');
        $imagefiles = [];
        $suppliers = Supplier::orderBy('supplierName')->get();

        $Item = Item::where('objectId', $code)->first();
        $last_search = session('last_search');


        if ($Item) {

            $itemCode = $Item['itemCode'];

            // Get Preferred Supplier details

            $sprefercost = ItemSupplier::select('ocl_itemsupplier.*', 'ocl_supplier.*')
                ->leftJoin('ocl_supplier', 'ocl_itemsupplier.supplierCode', 'ocl_supplier.supplierCode')
                ->where('ocl_itemsupplier.itemCode', '=', $itemCode)
                ->where('ocl_itemsupplier.productStatus', '=', 'active')
                ->orderBy('ocl_itemsupplier.supplierItemPriority', 'ASC')
                ->first();

            if ($Item['GSTTaxableFlag'] == 'yes') {
                $gst = 1.1;
            } else {
                $gst = 1;
            }

            if (is_dir("image/imageItemServer/" . $itemCode)) {
                $image = asset("image/imageItemServer/" . $itemCode . "/" . $itemCode . ".jpg");

                if ($handle = opendir(public_path('image/' . "imageItemServer/" . $itemCode . "/"))) {

                    while (false !== ($entry = readdir($handle))) {
                        if ($entry != "." && $entry != "..") {
                            $imagefiles[] = array('image' => asset("image/imageItemServer/" . $itemCode . "/" . $entry));
                        }
                    }
                    closedir($handle);
                }
            }
//            if ($handle = opendir(public_path('image/' . "imageItemServer/" . $itemCode . "/"))) {
//
//                while (false !== ($entry = readdir($handle))) {
//                    if ($entry != "." && $entry != "..") {
//                        $imagefiles[] = array('image' => asset("image/imageItemServer/" . $itemCode . "/" . $entry));
//                    }
//                }
//                closedir($handle);
//            }


            // Retail prices

            $rpresult = Object72::select('object_72.price', 'object_70.priceTypeName')
                ->leftJoin('object_70', 'object_70.oo_id', 'object_72.priceTypeObject__id')
                ->where('object_72.itemObject__id', '=', $code)
                ->where('object_70.priceTypeName', 'Like', '%retail%')
                ->get();

            $num_rprows = count($rpresult);


            // Competitor prices
            $cpresult = \DB::select("select o74.competitorName, o75.price, o28.ERPWebUOM, o75.competitorSKU, o75.dateRecorded from(
			 select o75x.competitorObject__id, o75x.pricingSellUOM__id, o75x.competitorSKU, max(o75x.dateRecorded) as dateRecorded
			 from object_75 as o75x
			 where o75x.itemObject__id=".$code." 
			 group by o75x.competitorObject__id,o75x.pricingSellUOM__id, o75x.competitorSKU) as x
			inner join object_75 as o75 on o75.competitorObject__id=x.competitorObject__id and o75.pricingSellUOM__id=x.pricingSellUOM__id and o75.competitorSKU= x.competitorSKU and o75.dateRecorded= x.dateRecorded
			left join object_74 as o74 on o74.oo_id = o75.competitorObject__id
			left join object_28 AS o28 ON o28.oo_id = o75.pricingSellUOM__id");

            $num_cprows = count($cpresult);

            // Promotion

            $promresult = Object65::where('oo_id', 660467)->first();
            $num_promrows = $promresult ? 1 : 0;

            if ($num_promrows > 0) {
                $promoId = $promresult['oo_id'];
                $promoCode = $promresult['promoCode'];

                // Test if this item in involved in the current promotion
                $promoType = '';

                if (strpos($Item['promoCampaign'], $promoCode) !== false) {
                    if ($Item['productType'] == 'promokit') {
                        $promoType = 'Promo Kit';
                        // Get all components of the kit

                        $pritemresult = \DB::select("select kitQuantity,kitCore,kitPosNote, o20p.oo_id as parentId,o20p.itemCode as parentItem,o20p.descriptionone as parentDescription1,o20p.descriptiontwo as parentDescription2,o20p.descriptionthree as parentDescription3, o20c.oo_id as childId,o20c.itemCode as childItem,o20c.descriptionone as childDescription1,o20c.descriptiontwo as childDescription2,o20c.descriptionthree as childDescription3 from object_66 AS o66 LEFT JOIN object_20 AS o20p ON o20p.oo_id = CONVERT (TRIM(BOTH ',' FROM o66.kitParent) , UNSIGNED) LEFT JOIN object_20 AS o20c ON o20c.oo_id = CONVERT (TRIM(BOTH ',' FROM o66.kitChild) , UNSIGNED) where o66.kitParent like '%$code%' and o20p.promoId like '%$promoId%'");
                        $num_pritemrows = count($pritemresult);
                    } else {
                        $promoType = 'Promo Item';
                    }

                    // This item is flagged for promo so it should have a price. Need to get the cost (if a specific supplier has been entered.)
                    $promoPrice = $Item['promoPrice'];
                    $promoCost = 0;
                    if (is_null($Item["promoSupplier"])) {
                        // Supplier Item defaults
                        $promoCost = $sprefercost['itemCost6'];
                        $promoCostDiv = $sprefercost['divisor'];
                        $promoSupplier = $sprefercost['supplierCode'];
                        $promoSupplierName = $sprefercost['supplierName'];
                    } else {
                        // Get Promo supplier details


                        $prsupresult = ItemSupplier::select('ocl_itemsupplier.*')
                            ->leftJoin('ocl_supplier', 'ocl_supplier.supplierCode', 'ocl_itemsupplier.supplierCode')
                            ->where('ocl_itemsupplier.objectId', '=', $code)
                            ->where('ocl_itemsupplier.supplierCode', '=', $Item['promoSupplier'])
                            ->where('ocl_itemsupplier.productStatus', '=', 'active')
                            ->orderBy('ocl_itemsupplier.supplierItemPriority', 'ASC')
                            ->first();
                        $num_prsuprows = count((array)$prsupresult);
                        if ($num_prsuprows > 0) {
                            $promoCost = $prsupresult['itemCost6'];
                            $promoCostDiv = $prsupresult['divisor'];
                            $promoSupplier = $prsupresult['supplierCode'];
                            $promoSupplierName = $prsupresult['supplierName'];
                        }
                    }
                } else {
                    // Check if item is a component of a kit

                    $pritemresult = Object66::select('object_66.kitQuantity', 'object_66.kitCore', 'object_66.kitPosNote', 'object_20.oo_id as parentId', 'object_20.itemCode as parentItem', 'object_20.descriptionone as parentDescription1', 'object_20.descriptiontwo as parentDescription2', 'object_20.descriptionthree as parentDescription3')
                        ->leftJoin('object_20', 'object_20.oo_id', \DB::raw("CONVERT (TRIM(BOTH ',' FROM object_66.kitParent) , UNSIGNED)"))
                        ->where('object_66.kitChild', 'Like', '%' . $code . '%')
                        ->where('object_20.promoId', 'Like', '%' . $promoId . '%')
                        ->get();
                    $num_pritemrows = count($pritemresult);
                    if ($num_pritemrows > 0) {
                        $promoType = 'Child';
                    }
                }
            }

            $presult = ItemSupplier::select('ocl_itemsupplier.*', 'ocl_supplier.*')
                ->leftJoin('ocl_supplier', 'ocl_supplier.supplierCode', 'ocl_itemsupplier.supplierCode')
                ->where('ocl_itemsupplier.productStatus', '=', 'active')
                ->where('ocl_itemsupplier.itemCode', '=', $Item->itemCode)
                ->orderBy('ocl_itemsupplier.supplierItemPriority', 'ASC')
                ->get();

        }


        if ($Item) {
            return view('products.details',
                compact('code', 'presult', 'suppliers', 'last_search', 'Item', 'sprefercost', 'gst',
                    'image', 'imagefiles', 'rpresult', 'num_rprows', 'cpresult', 'num_cprows', 'promresult', 'num_promrows', 'promoId', 'promoCode', 'promoType',
                    'pritemresult', 'num_pritemrows', 'promoCost', 'promoCostDiv', 'promoSupplier', 'promoSupplierName', 'promoPrice', 'prsupresult', 'num_prsuprows'
                ));
        } else {
            return redirect()->route('products');
        }


    }


}
