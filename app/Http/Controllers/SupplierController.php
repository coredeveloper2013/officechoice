<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemSupplier;
use App\Object20;
use App\Object65;
use App\Object66;
use App\Object72;
use App\Supplier;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\File;

class SupplierController extends Controller
{

    public function details($suppliercode , $supplieritem ,Request $request){


        $image = asset('image/NoImage.jpg');
        $imagefiles = [];

        $suppliers = Supplier::orderBy('supplierName')->get();
        $last_search = session('last_search');


        $dbdata= ItemSupplier::select('ocl_itemsupplier.*' , 'ocl_supplier.*')
            ->leftJoin('ocl_supplier', 'ocl_supplier.supplierCode', 'ocl_itemsupplier.supplierCode')
            ->leftJoin('ocl_item', 'ocl_item.itemCode', 'ocl_itemsupplier.itemCode')
            ->where('ocl_supplier.supplierCode', '=' ,$suppliercode)
            ->where('ocl_itemsupplier.itemSupplierCode', '=' ,$supplieritem)
            ->first();

        if($dbdata){
            $itemCode =$request->itemCode; //object id
//            $objectId = $dbdata['objectId'] ;

            $Item = Item::where('objectId', $itemCode)->first();
            // Get preferred supplier cost (for print function)

            $sprefercost = ItemSupplier::where('itemCode' , $itemCode)->where('productStatus' , 'active')->orderBy('supplierItemPriority')->first();

            if($dbdata['scDateFrom']){
                $scDateFrom  = date("d-M-Y", intval($dbdata['scDateFrom']));
            } else {
                $scDateFrom= '';
            }

            if($dbdata['scDateTo']){
                $scDateTo  = date("d-M-Y", intval($dbdata['scDateTo']));
            } else {
                $scDateTo= '';
            }

            if($dbdata['fcDate']){
                $fcDate  = date("d-M-Y", intval($dbdata['fcDate']));
            } else {
                $fcDate= '';
            }

            if (is_dir("image/imageItemServer/" . $Item->itemCode)) {
                $image =  asset("image/imageItemServer/".$Item->itemCode."/".$Item->itemCode.".jpg");

                if ($handle = opendir(public_path('image/'."imageItemServer/".$Item->itemCode."/"))) {
                    while (false !== ($entry = readdir($handle))) {
                        if ($entry != "." && $entry != "..") {
                            $imagefiles[]= array('image' => asset("image/imageItemServer/".$Item->itemCode."/".$entry));
                        }
                    }
                    closedir($handle);
                }
            }

            $histType  = array_reverse(explode(',', $dbdata['histType']));
            $histDate =  array_reverse(explode(',', $dbdata['histDate']));
            $histOld =  array_reverse(explode(',', $dbdata['histOld']));
            $histNew =  array_reverse(explode(',', $dbdata['histNew']));
        }



        if($dbdata){
            return view('supplier.details' ,
                compact('last_search','suppliers','suppliercode','supplieritem',
                    'supplieritem','image','imagefiles','dbdata','itemCode','objectId','sprefercost','scDateFrom',
                    'scDateTo','fcDate','histType','histDate','histOld','histNew','Item'));
        }  else {
            return redirect()->route('products');
        }

    }




}
