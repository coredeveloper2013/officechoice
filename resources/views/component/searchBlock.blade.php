<div class="col-md-12">
    <div class="contant">
        <div class="row">
            <hr class="product-up">
            <div class="col-md-2 col-xs-6">
                <div class="form-group input-group input-group-sm">
                    <label class="input-group-addon" for="input-sort">Sort By:</label>

                    <select id="sorting" class="form-control">


                        <option value="its.itemcode ASC" selected="selected">Product Code (A to Z)</option>
                        <option value="its.itemcode DESC">Product Code (Z to A)</option>
                        <option value="its.buyUom ASC">UOM(A to Z)</option>
                        <option value="its.buyUom DESC">UOM(Z to A)</option>
                        <option value="its.supplierCode ASC">Supplier(A to Z)</option>
                        <option value="its.supplierCode DESC">Supplier(Z to A)</option>
                        <option value="it.internetItemName ASC">Description(A to Z)</option>
                        <option value="it.internetItemName DESC">Description(Z to A)</option>
                        <option value="its.itemCost1 DESC">Cost(High to Low)</option>
                        <option value="its.itemCost1 ASC">Cost(Low to High)</option>






                    </select>
                </div>
            </div>

            <div class="col-md-2 col-xs-6">
                <div class="form-group input-group input-group-sm">
                    <label class="input-group-addon" for="input-sort">Result per page:</label>
                    <select id="input-limit" class="limit form-control">
                        <option value="10">10</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="500">500</option>


                    </select>
                </div>
            </div>


            <div class="col-md-1 col-xs-6 pull-right totpag verticalmiddle pd-left-no">
                of 0
            </div>

            <div class="col-md-1 col-xs-6 pull-right">
                <div class="form-group input-group input-group-sm">
                    <label class="input-group-addon" for="input-sort">Page:</label>
                    <select class="paging form-control">
                        <option value="1">1</option>
                    </select>

                </div>
            </div>

            <div class="col-md-1 col-xs-6 pull-right arrows pd-left-no">
                <div class="arprevl"><img style="display:none" id="arprev" class="arprev" src="{{asset('office/image/arrow-left.png')}}"></div>
                <div class="arnextl"><img style="display:none" id="arnext" class="arnext" src="{{asset('office/image/arrow-right.png')}}"></div>

            </div>

            <div class="col-md-2 col-xs-6 pull-right">
                <span> <button class="btn btn-primary productcount">0</button>Product found</span>
            </div>
        </div>





        <div class="row fully-bold">

            <div class="col-md-1">
                &nbsp
            </div>

            <div class="col-md-2">
                Product
            </div>

            <div class="col-md-4">
                Description
            </div>

            <div class="col-md-1">
                UOM
            </div>

            <div class="col-md-2">
                Supplier-item
            </div>

            <div class="col-md-1">
                Cost
            </div>

            <div class="col-md-1">
                Status
            </div>


        </div>




        <div class="row">

            <div id="table" class="border-top fully-bold">
                <div class="row">
                    <div class="col-md-12">
                        <p class="bold-style">Type in any part of a product description to begin your search.</p>
                    </div>
                </div>
            </div>

        </div>



        <div class="row">



            <div class="col-md-2 col-xs-6">

            </div>

            <div class="col-md-2 col-xs-6">

            </div>


            <div class="col-md-1 col-xs-6 pull-right totpag verticalmiddle pd-left-no">
                of 0
            </div>


            <div class="col-md-1 col-xs-6 pull-right">
                <div class="form-group input-group input-group-sm">
                    <label class="input-group-addon" for="input-sort">Page:</label>
                    <select class="paging form-control">
                        <option value="1">1</option>
                    </select>
                </div>
            </div>



            <div class="col-md-1 col-xs-6 pull-right arrows pd-left-no">
                <div class="arprevl"><img style="display:none" id="arprev" class="arprev" src="{{asset('image/arrow-left.png')}}">
                </div>
                <div class="arnextl"><img style="display:none" id="arnext" class="arnext" src="{{asset('image/arrow-right.png')}}">
                </div>

            </div>

            <div class="col-md-2 col-xs-6 pull-right">
                <span> <button class="btn btn-primary productcount">0</button>Product found</span>
            </div>


        </div>



    </div>
</div>
