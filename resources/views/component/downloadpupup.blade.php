<div class="has">
    <div class="panel panel-primary">

        <div class="panel-heading"><i class="btn btn-danger" style="float:right;margin-top:0px" onclick="fun22();">X</i>     <P>File Download Request</P></div>

        <div class="panel-body">
            <strong> File Type : </strong><select class="form-control" name="keys" id="key">
                <option value="">Select </option>
                <option value="1">Managed Range </option>
                <option value="2">Education Range </option>
                <option value="3">Sort File</option>
                <option value="4">Supplier Price List </option>
                <option value="5">Future Price List </option>
            </select>
            <div class="1 table table-stripped">
                <table class="table table-stripped">
                    <thead>
                    <tr> <td>Branded</td><td>Siblings</td><td>FLC Only</td>  </tr>
                    </thead>
                    <tbody>
                    <tr class="">
                        <form>
                            <td><input type="checkbox" name="chkBrand" id="chkBrand"/></td>
                            <td><input type="checkbox" name="chkSiblings" id="chkSiblings"/></td>
                            <td><input type="checkbox" name="chkFLC" id="chkFLC"/></td>
                        </form>
                    </tr>

                    </tbody>
                </table>
                <div class="download"></div>
            </div>
            <div class="2 table table-stripped">
                <table class="table table-stripped">
                    <thead>
                    <tr> 	<td>Branded</td> <td>Un Branded</td> </tr>
                    </thead>
                    <tbody>

                    <tr class="">
                        <form>
                            <td><input type="checkbox" name="chkEducation" id="chkEducation"/></td> <td><input type="checkbox" name="chkEducation1" id="chkEducation1"/></td> </form>
                    </tr>
                    </tbody>
                </table>
                <div class="download1"></div>
            </div>
            <div class="3 table table-stripped">
                <table class="table table-stripped">
                    <thead>
                    <tr><td>Branded</td> 	<td>Un Branded</td>  </tr>
                    </thead>
                    <tbody>
                    <tr class="">
                        <form>
                            <td><input type="checkbox" name="chkSort" id="chkSort"/></td>
                            <td><input type="checkbox" name="chkSort1" id="chkSort1"/></td>
                        </form>
                    </tr>
                    </tbody>
                </table>
                <div class="download2"></div>
            </div>
            <div class="4 table table-stripped">
                <br />
                <div>
                    <strong>Select a Supplier : </strong><br /><select id="ddlSupplier" class="form-control">
                        <option value="select">Select</option>
                        <option value="all">All</option>
                        @foreach($suppliers as $row)
                        <option value="{{ $row['supplierCode'] }}">{{ $row['supplierName'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="download3"></div>
            </div>

            <div class="5 table table-stripped">
                <br />
                <div>
                    <strong>Select a Supplier : </strong><br /><select id="ddlFuture" class="form-control">
                        <option value="select">Select</option>
                        <option value="all">All</option>

                        @foreach($suppliers as $row)
                        <option value="{{ $row['supplierCode'] }}">{{  $row['supplierName'] }}</option>
                            @endforeach




                    </select>
                </div>

                <div class="download4"></div>
            </div>
        </div>
    </div>
</div>