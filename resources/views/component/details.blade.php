<div class="breadcrumb"><b>
        <a href="{{ route('products')  }}">Home</a> >
        <a href="{{ route('product.details' , ['code' => $code])  }}"
           class="text-danger"> {{$Item->internetItemName}}  </a></b>
</div>


<div class="contant">
    <div class="row">
        <div class="product-details">
            <!-- Left Page Column -->
            <div class="product-image col-md-3 col-sm-12">

                <!-- Images -->
                <div class="thumbnil">

                    <!-- Main Image -->
                    <div class="image">
                        {{--                        <span class="zoom-icon"> <img src="./icon/zoom-icon.png" alt="zoom image" title="zoom image"> Zoom</span>--}}
                        @if(file_exists(public_path().'/image/imageItemServer/' . $Item->itemCode . '/' . $Item->itemCode . '.jpg'))
                            <img class="zoom_image" id="img_01"
                                 src="{{asset('image/imageItemServer/' . $Item->itemCode . '/' . $Item->itemCode . '.jpg')}}"
                                 data-zoom-image="{{asset('image/imageItemServer/' . $Item->itemCode . '/' . $Item->itemCode . '.jpg')}}"/>
                        @else
                            <img class="zoom_image" id="img_01"
                                 src="{{asset('image/imageItemServer/NoImage.jpg')}}"
                                 data-zoom-image="{{asset('image/imageItemServer/NoImage.jpg')}}"/>
                        @endif
                    </div>
                    <!-- More Image Gallery -->
                    <div class="additional_image" id="additional_image">
                        @foreach($imagefiles as $image)
                            <div>
                                <a href="#" data-image="{{$image['image']}}" data-zoom-image="{{$image['image']}}">
                                    <img id="img_01" src='{{$image['image']}}' data-zoom-image="{{$image['image']}}"/>
                                </a>

                            </div>
                        @endforeach
                    </div>

                    <script>
                        $("#img_01").elevateZoom({
                            gallery: 'additional_image',
                            cursor: 'pointer',
                            galleryActiveClass: 'active',
                            imageCrossfade: true,
                            zoomType: 'window'
                        });

                        //pass the images to Fancybox
                        $("#img_01").bind("click", function (e) {
                            var ez = $('#img_01').data('elevateZoom');
                            $.fancybox(ez.getGalleryList());
                            return false;
                        });

                        $(document).ready(function () {
                            $('.additional_image').bxSlider({
                                slideWidth: 80,
                                minSlides: 2,
                                maxSlides: 3,
                                slideMargin: 10,
                            });
                        });
                    </script>
                </div>


                <div class="bottom-link">
                    <table width="100%" border="0">
                        <thead>
                        <tr>
                            <td>Video</td>
                            <td>MSDS</td>
                            <td>Link</td>
                        </tr>
                        </thead>
                        <tr>

                            <td>
                                @if($Item->productVideoLink)
                                    <a href="{{$Item->productVideoLink}}">
                                        <img class="iconsize" src="{{asset('office/image/video.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>
                                    </a>
                                @else

                                    <img class="iconsize" src="{{asset('office/image/video.jpg')}}"
                                         alt="No Content Available" title="No Content Available"/>
                                @endif

                            </td>
                            <td>
                                @if($Item->PDFLink)
                                    <a href="{{$Item->PDFLink}}">
                                        <img class="iconsize" src="{{asset('office/image/msds.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>
                                    </a>
                                @else
                                    <img class="iconsize" src="{{asset('office/image/msds.jpg')}}"
                                         alt="No Content Available" title="No Content Available"/>
                                @endif


                            </td>
                            <td>
                                @if($Item->productWebLink)
                                    <a href="{{$Item->productWebLink}}">
                                        <img class="iconsize" src="{{asset('office/image/link.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>
                                    </a>
                                @else

                                    <img class="iconsize" src="{{asset('office/image/link.jpg')}}"
                                         alt="No Content Available" title="No Content Available"/>
                                @endif

                            </td>
                        </tr>
                    </table>
                </div>

                <!-- Product Status Icons -->
                <div class="bottom-status">
                    <table width="100%" border="0">
                        <thead>
                        <tr>
                            <td>Managed</td>
                            <td>OC Only</td>
                            <td>Status</td>
                            <td>Consumable</td>
                            <td>Print</td>
                        </tr>
                        </thead>
                        <tr>
                            <td>
                                @if($Item->managedRangeFlag == 'yes')
                                    <img class="iconsize2" alt="Managed" title="Not Managed"
                                         src="{{asset('office/image/right-sign.jpg')}}"/>
                                @else
                                    <img class="iconsize2" alt="Not Managed" title="Not Managed"
                                         src="{{asset('office/image/cross-sign.jpg')}}"/>
                                @endif
                            </td>
                            <td>
                                @if($Item->availability == 'oconly')
                                    <img class="iconsize2" alt="Managed" title="Not Managed"
                                         src="{{asset('office/image/right-sign.jpg')}}"/>
                                @else
                                    <img class="iconsize2" alt="Not Managed" title="Not Managed"
                                         src="{{asset('office/image/cross-sign.jpg')}}"/>
                                @endif
                            </td>
                            <td>
                                @if($Item->productStatus == 'active')
                                    <img class="iconsize2" alt="Is active" title="Is active"
                                         src="{{asset('office/image/right-sign.jpg')}}"/>
                                @else
                                    <img class="iconsize2" alt="Not active" title="Not active"
                                         src="{{asset('office/image/cross-sign.jpg')}}"/>
                                @endif
                            </td>
                            <td>
                                @if($Item->consumableFlag == 'yes')
                                    <img class="iconsize2" alt="Consumable" title="Consumable"
                                         src="{{asset('office/image/right-sign.jpg')}}"/>
                                @else
                                    <img class="iconsize2" alt="Not Consumable" title="Not Consumable"
                                         src="{{asset('office/image/cross-sign.jpg')}}"/>
                                @endif
                            </td>
                            <td>
                                <img style="padding-top:5px" class="iconsize2 print" alt="Print"
                                     onClick="printdiv('print-div');" title="Print"
                                     src="{{asset('office/image/icon-print.png')}}"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- Product Description -->
            <div class="product-details col-md-9 col-sm-12">
                <h2 class="product-name">{{$Item->internetItemName}}</h2>

                <div class="clear"></div>
                <ul class="list-inline bottol-border">
                    <li><strong>OC Code :</strong> {{$Item->objectId}}</li>
                    <li><strong>Barcode :</strong> {{$Item->barcodeSellUOM}}</li>
                    <li><strong>Manf :</strong> {{$Item->manufacturersItemCode}}</li>
                    <li><strong>Old Code :</strong> {{$Item->itemCode}}</li>
                </ul>

                <div class="clear"></div>
                <div class="det-left col-md-7 col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-basic" data-toggle="tab">Basic</a></li>
                        <li><a href="#tab-advanced" data-toggle="tab">Advanced</a></li>
                        @if ($promoType<>'')
                            <li><a href="#tab-promo" data-toggle="tab">Promotion</a></li>
                        @endif
                    </ul>

                    <!-- TABS -->
                    <div class="tab-content">

                        <!-- Basic Tab -->
                        <div class="tab-pane active" id="tab-basic">
                            <div class="clear"></div>
                            <table width="100%" border="0" class="skip-space">
                                <tr>
                                    <td><strong>UOM :</strong></td>
                                    <td>{{$Item->pricingSellUOM}}</td>
                                    <td><strong>Weight :</strong></td>
                                    <td> {{$Item->productWeight ? $Item->productWeight : ''}} </td>
                                    <td><strong>Dimension :</strong></td>
                                    <td> {{$Item->productLength ? $Item['productLength'] . "x". $Item['productWidth']. "x". $Item['productHeight'] . "cm" : ''}} </td>
                                </tr>
                                <tr>
                                    <td><strong>FLC :</strong></td>
                                    <td>{{$Item->isFLCFlag == 'yes' ? 'Page '.$Item['FLCERPPage'] : 'no'}}</td>
                                    <td><strong>EDU :</strong></td>
                                    <td>{{$Item->EDUFlag}}</td>
                                    <td>
                                        <strong> {{$Item->dateItemInactive ? 'Inactive: ' : 'Active : '}}</strong>
                                    </td>
                                    <td>{{$Item->dateItemInactive ? date("d-M-y",  intval($Item['dateItemInactive'])) : date("d-M-y", intval($Item['dateItemActive']))}}                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="pt15"><strong>Internet Menu
                                            : {{ $Item->internetMenu  }}</strong></td>
                                </tr>
                            </table>

                            <div class="clear"></div>
                            <p>
                            <ul class="bulletdesc">
                                <?php if ($Item['bulletPointOne'] && $Item['bulletPointOne'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointOne'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointTwo'] && $Item['bulletPointTwo'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointTwo'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointThree'] && $Item['bulletPointThree'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointThree'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointFour'] && $Item['bulletPointFour'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointFour'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointFive'] && $Item['bulletPointFive'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointFive'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointSix'] && $Item['bulletPointSix'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointSix'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointSeven'] && $Item['bulletPointSeven'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointSeven'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointEight'] && $Item['bulletPointEight'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointEight'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointNine'] && $Item['bulletPointNine'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointNine'] . '</li>';
                                } ?>
                                <?php if ($Item['bulletPointTen'] && $Item['bulletPointTen'] <> ' ') {
                                    echo "<li>" . $Item['bulletPointTen'] . '</li>';
                                } ?>
                            </ul>
                            <br>
                        </div>

                    @php
                        $divi = 0;
                    @endphp

                    <!-- Advanced Tab -->
                        <div class="tab-pane" id="tab-advanced">
                            <div class="clear"></div>
                            <table width="100%" border="0" class="cost-price">
                                <thead>
                                <tr>
                                    <td colspan="2"><strong>Meta Data</strong></td>
                                    <td colspan="2"><strong>Merchandise Hierarchy</strong></td>
                                </tr>
                                </thead>
                                <tr>
                                    <td><strong>Product Type:</strong></td>
                                    <td><?php echo $Item['internetMetaDataProduct']; ?></td>
                                    <td><strong>Department:</strong></td>
                                    <td><?php echo $Item['merchandisingDepartment']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Product Cat:</strong></td>
                                    <td><?php echo $Item['internetMetaDataProductCategory']; ?></td>
                                    <td><strong>Major Class:</strong></td>
                                    <td><?php echo $Item['merchandisingMajorClass']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Brand:</strong></td>
                                    <td><?php echo $Item['internetMetaDataBrand']; ?></td>
                                    <td><strong>Minor Class:</strong></td>
                                    <td><?php echo $Item['merchandisingMinorClass']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Colour:</strong></td>
                                    <td><?php echo $Item['internetMetaDataColour']; ?></td>
                                    <td><strong>Sub Class:</strong></td>
                                    <td><?php echo $Item['merchandisingSubClass']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Size</strong></td>
                                    <td><?php echo $Item['internetMetaDataSize']; ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Weight:</strong></td>
                                    <td><?php if ($Item['internetMetaDataWeight']) {
                                            echo $Item['internetMetaDataWeight'];
                                            echo "gm";
                                        } ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Capacity:</strong></td>
                                    <td><?php echo $Item['internetMetaDataCapacity']; ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Dimensions:</strong></td>
                                    <td><?php echo $Item['internetMetaDataDimension']; ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><strong>Certification:</strong></td>
                                    <td><?php echo $Item['internetMetaDataCertification']; ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td><strong>Pack Qty:</strong></td>
                                    <td><?php echo $Item['internetMetaDataPackQty']; ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                            </table>
                        </div>

                        <!-- Promotion Tab -->
                        @if($promoType <> '')
                            <div class="tab-pane" id="tab-promo">
                                <div class="clear"></div>
                                <table width="100%" border="0" class="skip-space">
                                    <tr>
                                        <td colspan="6" class="pt15"><strong>Promotion
                                                :</strong> {{$promresult['promoDescription']}}</td>
                                    </tr>
                                    <!--<tr><td>&nbsp;</td></tr>
                                    <div class="clear"> </div><p>-->
                                    <tr>
                                        <td><strong>Start :</strong></td>
                                        <td>{{date("d-M-y", strtotime('+1 day', $promresult['promoStartDate']))}}</td>
                                        <td><strong>&nbsp;</strong></td>
                                        <td> &nbsp;</td>
                                        <td><strong>Buy Start :</strong></td>
                                        <td>{{date("d-M-y", strtotime('+1 day', $promresult['promoBuyStartDate']))}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish :</strong></td>
                                        <td>{{date("d-M-y", strtotime('+1 day', $promresult['promoEndDate']))}}</td>
                                        <td><strong>&nbsp;</strong></td>
                                        <td>&nbsp;</td>
                                        <td><strong>Buy Finish :</strong></td>
                                        <td>{{date("d-M-y", strtotime('+1 day', $promresult['promoBuyEndDate']))}}</td>
                                    </tr>

                                    <!-- Show price/cost -->
                                    @if($promoPrice > 0)
                                        <tr>
                                            <td><strong>Sell Price :</strong></td>
                                            <td>${{sprintf("%.2f", $promoPrice)}}</td>
                                            <td><strong>&nbsp;</strong></td>
                                            <td>&nbsp;</td>
                                            <td><strong>Buy Price :</strong></td>
                                            <td>$ {{ sprintf("%.2f", $promoCost)}}</td>
                                        </tr>
                                    @endif
                                </table>

                                <div class="clear"></div>
                                <p>

                                    <!-- Kit display -->
                                    @if($promoType == 'Promo Kit')
                                        <tr>&nbsp;</tr>
                                <table width="100%" border="0" class="skip-space">
                                    <tr>
                                        <td><strong>Bundle/Kit</strong></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    @if($num_pritemrows > 0)
                                        <tr>
                                            <td>Components</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        @foreach($pritemresult as $pritemrow)
                                            <tr>
                                                <td>x {{$pritemrow->kitQuantity}}</td>
                                                <td>
                                                    <a href="./detail.php?code={{$pritemrow->childId}}">{{$pritemrow->childId}}</a>
                                                </td>
                                                <td><?php echo $pritemrow->childDescription1 . ' ' . $pritemrow->childDescription2 . ' ' . $pritemrow->childDescription3; ?></td>
                                                <td><strong>
                                                        <?php if ($pritemrow->kitCore == 'No') {
                                                            echo 'Bonus';
                                                        } else {
                                                            echo 'Core';
                                                        } ?></strong></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>No kit components</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    @endif
                                </table>
                            @endif


                            <!-- Child/Component display -->
                                <?php if($promoType == 'Child') { ?>
                                <tr>&nbsp;</tr>
                                <table width="100%" border="0" class="skip-space">
                                    <tr>
                                        <td colspan="6"><strong>Part of a Kit</strong></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <?php if($num_pritemrows > 0){ ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <?php foreach($pritemresult as $pritemrow) { ?>
                                    <tr>
                                        <td nowrap><?php echo 'x ' . $pritemrow['kitQuantity']; ?></td>
                                        <td>
                                            <a href="./detail.php?code=<?php echo $pritemrow['parentId']; ?>"><?php echo $pritemrow['parentId']; ?></a>
                                        </td>
                                        <td><?php echo $pritemrow['parentDescription1'] . ' ' . $pritemrow['parentDescription2']; ?></td>
                                        <td><strong><?php if ($pritemrow['kitCore'] == 'No') {
                                                    echo 'Bonus';
                                                } else {
                                                    echo 'Core';
                                                } ?></strong></td>
                                    </tr>
                                    <?php } ?>
                                    <?php } else { ?>
                                    <tr>
                                        <td>No kits defined</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php } ?>
                                </table>
                                <?php } ?>


                                <br>
                            </div>
                    @endif

                    <!-- Available Suppliers List -->
                        <div class="available-suppliers">
                            <table width="100%" border="0">
                                <thead>
                                <tr>
                                    <td colspan="9" class="text-center"><strong>AVAILABLE SUPPLIERS</strong></td>
                                </tr>
                                </thead>
                                <thead>
                                <tr>
                                    <td>Supplier</td>
                                    <td>Code</td>
                                    <td>Uom</td>
                                    <td>MOQ</td>
                                    <td>Div.</td>
                                    <td>Cost</td>
                                    <td>Unit</td>
                                    <td>Special</td>
                                    <td>From</td>
                                    <td>To</td>
                                </tr>
                                </thead>

                                <?php

                                foreach($presult as $sellerrow) {
                                $seller = $sellerrow;
                                if ($seller['scDateTo']) {
                                    $scDateTo = date("d-M-y", $seller['scDateTo']);
                                } else {
                                    $scDateTo = '';
                                }
                                if ($seller['scDateFrom']) {
                                    $scDateFrom = date("d-M-y", $seller['scDateFrom']);
                                } else {
                                    $scDateFrom = '';
                                }
                                ?>
                                <tr>
                                    <td>
                                        <a href="{{route('supplier-item' , ['suppliercode' => $seller['supplierCode'] , 'supplieritem' => $seller['itemSupplierCode'],'itemCode'=>$code ])}}"><?php echo $seller['supplierName']; ?></a>
                                    </td>
                                    <td><?php echo $seller['itemSupplierCode']; ?></td>
                                    <td><?php echo $seller['buyUom']; ?></td>
                                    <td><?php echo $seller['moq']; ?></td>
                                    <td><?php echo $seller['divisor']; ?></td>
                                    <td><?php $num = $seller['itemCost1']; if ($num > 0) {
                                            echo "$";
                                            echo sprintf("%.2f", $num);
                                        } ?></td>
                                    <td><strong><?php $num = $seller['itemCost1'] / $seller['divisor']; if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></strong></td>
                                    <td><?php $num = $seller['specialCost']; if ($num > 0) {
                                            echo "$";
                                            echo sprintf("%.2f", $num);
                                        } ?></td>
                                    <td><?php echo $scDateFrom; ?></td>
                                    <td><?php echo $scDateTo; ?></td>
                                </tr>
                                <?php } ?>

                            </table>
                        </div>
                    </div>
                </div>

                <!-- Pricing Section -->
                <!-- Pricing Section -->
                <div class="det-rightx auto-width col-md-5 col-sm-12">
                    <h4 class="price">Pricing</h4>
                    <hr/>
                    <h4 class="price all-caps">Suggested Sell Pricing</h4>

                    @php
                        $divideBY = isset($sprefercost['itemCost1']) ? $sprefercost['itemCost1']/$sprefercost['divisor'] :  0;
                        $priceBreakOne = $Item['priceBreakOne'] ? $Item['priceBreakOne'] : .11111;
                        $priceBreakTwo = $Item['priceBreakTwo'] ? $Item['priceBreakTwo'] : .11111;
                        $priceBreakThree = $Item['priceBreakThree'] ? $Item['priceBreakThree'] : .11111;
                        $priceBreakFour = $Item['priceBreakFour'] ? $Item['priceBreakFour'] : .11111;
                    @endphp

                    <table width="100%" border="0" class="pricing-table">
                        <thead>
                        <tr>
                            <td>Quantity Breaks</td>
                            <td><?php $num = $Item['quantityBreakOne']; if ($num > 0) {
                                    echo $num;
                                } ?></td>
                            <td><?php $num = $Item['quantityBreakTwo']; if ($num > 0) {
                                    echo $num;
                                } ?></td>
                            <td><?php $num = $Item['quantityBreakThree']; if ($num > 0) {
                                    echo $num;
                                } ?></td>
                            <td><?php $num = $Item['quantityBreakFour'];  if ($num > 0) {
                                    echo $num;
                                } ?></td>
                        </tr>
                        </thead>
                        <tr>
                            <td>Inc GST</td>
                            <td><?php $num = ($priceBreakOne * $gst); if ($num > 0 && $priceBreakOne != .11111) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = ($priceBreakTwo * $gst); if ($num > 0 && $priceBreakTwo != .11111) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = ($priceBreakThree * $gst); if ($num > 0 && $priceBreakThree != .11111) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = ($priceBreakFour * $gst); if ($num > 0 && $priceBreakFour != .11111) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                        </tr>
                        <tr>
                            <td>Ex GST</td>
                            <td><?php $num = $priceBreakOne; if ($num > 0 && $priceBreakOne != .11111) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $priceBreakTwo; if ($num > 0 && $priceBreakTwo != .11111) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $priceBreakThree; if ($num > 0 && $priceBreakThree != .11111) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $priceBreakFour; if ($num > 0 && $priceBreakFour != .11111) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                        </tr>
                        <tr>
                            <td>GP %</td>
                            <td>
                                <?php
                                $gp1 = 0;
                                if($sprefercost['divisor'] != null && $sprefercost['divisor'] != '' && $sprefercost['divisor'] == 0){
                                    $num = ($gp1= ($Item['priceBreakOne']-  ($sprefercost['itemCost1']/$sprefercost['divisor']))/$Item['priceBreakOne'])*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $gp2 = 0;
                                if($sprefercost['divisor'] != null && $sprefercost['divisor'] != '' && $sprefercost['divisor'] == 0){
                                    $num = ($gp2= ($Item['priceBreakTwo']-  ($sprefercost['itemCost1']/$sprefercost['divisor']))/$Item['priceBreakTwo'])*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $gp3 = 0;
                                if($sprefercost['divisor'] != null && $sprefercost['divisor'] != '' && $sprefercost['divisor'] == 0){
                                    $num = ($gp3= ($Item['priceBreakThree']-($sprefercost['itemCost1']/$sprefercost['divisor']))/$Item['priceBreakThree'])*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $gp4 = 0;
                                if($sprefercost['divisor'] != null && $sprefercost['divisor'] != '' && $sprefercost['divisor'] == 0){
                                    $num = ($gp4= ($Item['priceBreakFour']- ($sprefercost['itemCost1']/$sprefercost['divisor']))/$Item['priceBreakFour'])*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Average GP %</td>
                            <?php
                            $gpav = (($gp1 + $gp2 + $gp3 + $gp4) / 4) * 100;
                            if ($divi == 0) {
                                $divi = 1;
                            } ?>
                            <td colspan="4"><?php if ($gpav > 0) {
                                    echo number_format((float)$gpav, 2, '.', '');
                                    echo "%";
                                } ?></td>
                        </tr>
                    </table>
                    <br/>


                    <!-- Retail Pricing -->
                    <?php if ($Item['RRP'] > 0 || $num_rprows > 0 || $promoPrice > 0) { ?>
                    <table width="100%" border="0" class="pricing-table">
                        <thead>
                        <tr>
                            <td>Retail Pricing</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        </thead>
                        <tr>
                            <td>Manufacturer's RRP</td>
                            <td>
                                <?php if ($Item['RRP'] > 0) {
                                    $num = ($Item['RRP'] * $gst);
                                    echo "$";
                                    echo number_format((float)$num, 2, '.', '');
                                } else {
                                    echo '$ n/a';
                                }
                                ?>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php if ($num_rprows > 0) {
                        foreach($rpresult as $rprow) {
                        $rp = $rprow;
                        $gp = $rp['price'] != 0 ? (($rp['price'] - ($divideBY)) / $rp['price']) * 100 : 0 ?>
                        <tr>
                            <td><?php echo $rp['priceTypeName']; ?></td>
                            <td><?php echo '$' . sprintf("%.2f", $rp['price'] * $gst); ?></td>
                            <td>&nbsp;</td>
                            <td><?php echo number_format((float)$gp, 2, '.', ''); echo "% GP"; ?></td>
                            <!--<td>GP</td>-->
                        </tr>
                        <?php } ?>
                        <?php } ?>
                        <?php
                        if($promoPrice > 0) {
                        if ($promoCostDiv > 0) {
                            $gp = (($promoPrice - ($promoCost / $promoCostDiv)) / $promoPrice) * 100;
                        } else {
                            $gp = (($promoPrice - ($promoCost)) / $promoPrice) * 100;
                        }
                        ?>
                        <tr>
                            <td>Promotion Price</td>
                            <td><?php echo '$' . sprintf("%.2f", $promoPrice); ?></td>
                            <td>&nbsp;</td>
                            <td><?php echo number_format((float)$gp, 2, '.', ''); echo "% GP"; ?></td>
                            <!--<td>GP</td>-->
                        </tr>
                        <?php } ?>
                    </table>
                    <br>
                    <?php } ?>
                <!-- End Retail Pricing -->


                    <!-- Competitor Pricing -->
                    <?php if($num_cprows > 0) { ?>
                    <h4 class="price all-caps">Competitor Pricing</h4>

                    <table width="100%" border="0" class="pricing-table">
                        <thead>
                        <tr>
                            <td>Competitor</td>
                            <td>SKU</td>
                            <td>UOM</td>
                            <td>Price</td>
                            <td>Date</td>
                        </tr>
                        </thead>

                        <?php foreach($cpresult as $cprow) {
                        $cp = $cprow; ?>
                        <tr>
                            <td><?php echo $cp['competitorName']; ?></td>
                            <td><?php echo $cp['competitorSKU']; ?></td>
                            <td><?php echo $cp['ERPWebUOM']; ?></td>
                            <td><?php echo '$' . sprintf("%.2f", $cp['price']); ?></td>
                            <td><?php echo date("d-M", $cp['dateRecorded']); ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                    <br>
                    <?php } ?>
                <!-- End Competitor Pricing -->


                    <!-- Education Pricing -->
                    <?php if ($Item['educationPrice1'] > 0) { ?>
                    <h4 class="price all-caps">Education Pricing</h4>

                    <table width="100%" border="0" class="pricing-table">
                        <thead>
                        <tr>
                            <td>Levels</td>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                        </tr>
                        </thead>
                        <tr>
                            <td>Inc GST</td>
                            <td><?php $num = $Item['educationPrice1'] * $gst; if ($num > 0) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $Item['educationPrice2'] * $gst; if ($num > 0) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $Item['educationPrice3'] * $gst; if ($num > 0) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $Item['educationPrice4'] * $gst; if ($num > 0) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                        </tr>
                        <tr>
                            <td>Ex GST</td>
                            <td><?php $num = $Item['educationPrice1']; if ($num > 0) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $Item['educationPrice2']; if ($num > 0) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $Item['educationPrice3']; if ($num > 0) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                            <td><?php $num = $Item['educationPrice4']; if ($num > 0) {
                                    echo "$";
                                    echo sprintf("%.2f", $num);
                                } ?></td>
                        </tr>
                        <tr>
                            <td>GP %</td>
                            <?php $divi = 0; ?>
                            <td><?php $num = ($gp1 = ($Item['educationPrice1'] - ($divideBY)) / $Item['educationPrice1']) * 100; if ($num > 0) {
                                    echo number_format((float)$num, 2, '.', '');
                                    echo "%";
                                    $divi++;
                                } ?></td>
                            <td><?php $num = ($gp2 = ($Item['educationPrice2'] - ($divideBY)) / $Item['educationPrice2']) * 100; if ($num > 0) {
                                    echo number_format((float)$num, 2, '.', '');
                                    echo "%";
                                    $divi++;
                                } ?></td>
                            <td><?php $num = ($gp3 = ($Item['educationPrice3'] - ($divideBY)) / $Item['educationPrice3']) * 100; if ($num > 0) {
                                    echo number_format((float)$num, 2, '.', '');
                                    echo "%";
                                    $divi++;
                                } ?></td>
                            <td><?php $num = ($gp4 = ($Item['educationPrice4'] - ($divideBY)) / $Item['educationPrice4']) * 100; if ($num > 0) {
                                    echo number_format((float)$num, 2, '.', '');
                                    echo "%";
                                    $divi++;
                                } ?></td>
                        </tr>
                        <tr>
                            <td>Average GP %</td>
                            <?php if ($divi == 0) {
                                $divi = 1;
                            } ?>
                            <td colspan="4"><?php $num = (($gp1 + $gp2 + $gp3 + $gp4) / $divi) * 100; if ($num > 0) {
                                    echo number_format((float)$num, 2, '.', '');
                                    echo "%";
                                } ?></td>
                        </tr>
                    </table>
                <?php } ?>
                <!-- End Education Pricing -->


                </div>
            </div>
        </div>
    </div>


    <div class="download4"></div>
</div>
