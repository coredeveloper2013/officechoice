<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Office Choice</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel="stylesheet" href="{{asset('office/icons/themify-icons.css')}}">

<link rel="stylesheet" href="{{asset('office/css/default.css')}}">
<link rel="stylesheet" href="{{asset('office/css/custom.css')}}">
 <script src="{{asset('office/js/jquery-1.11.1.min.js')}}"></script>
 <!--<script src="assets/bootstrap/js/bootstrap.min.js"></script>-->
<script src="{{asset('office/js/jquery.backstretch.min.js')}}"></script>

    <style type="text/css">
        .invalid-feedback {
            font-size: 12px;
            color: #0b2e13;
        }

        .valid-feedback {
            font-weight: bold;
            color: green;
        }
    </style>

</head>
<body id="login-page">
  <header>
    <div class="center"> <img src="{{asset('office/image/logo.png')}}" /></div>
</header>
    <div class="container">
        <div class="contant">



        <form method="post" action="{{route('custom_login')}}" >

            {{csrf_field()}}

            <div class="login">

                                        
                <h1 class="center"> Login </h1>

                @if(session()->has('warning'))
                    <p class="invalid-feedback" style="text-align: center">
                        <strong>{{ session('warning') }}</strong>
                    </p>
                @endif



                <div class="login-well well">

                    @if (session()->has('success'))
                        <p class="valid-feedback">
                            <strong>{{ session('userPassword').session('success') }}</strong>
                        </p>
                    @endif
                <div> <h2> Username </h2> </div>
                <div><input name="userName" type="text" value="{{ old('userName') }}" placeholder="username" size="20" maxlength="40" /> </div>
                    @if ($errors->has('userName'))
                        <p class="invalid-feedback">
                            <strong>{{ $errors->first('userName') }}</strong>
                        </p>
                    @endif
                <div> <h2> Password </h2>   </div>
                <div><input name="userPassword" type="password" value="" size="20" maxlength="40" /> </div>
                    @if ($errors->has('userPassword'))
                        <p class="invalid-feedback">
                            <strong>{{ $errors->first('userPassword') }}</strong>
                        </p>
                @endif

                <!--<div class="full remember"> <div class="half cols">  <a href="forgot.php">Forget Password?</a> </div>-->



            <div class="full center loginbutton"><button type="submit" class="default" > login <span class="ti-angle-right"> </span> </button></div>
             <br />

           <div class="full center "> <a href=" {{ route('password.request') }} "> <p> Forgot Password ?</p> </a> </div>

                        <div class="full remember"> <div class="half cols">  <?php echo 'IP: '.$_SERVER['REMOTE_ADDR'];  ?> </div>

                </div>
            </div>
            </div>
            </form>

        </div>


        </div>
    </body>
