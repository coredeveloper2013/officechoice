@extends('master')


@section('page-title')

    Home
@endsection

@section('mainContent')

    @include('component.downloadpupup')


    <div class="container searchblock">
            @include('component.searchBlock')
    </div>


    @include('component.searchScript')

    <script>
        var current_string = '<?= $last_search ?>';

        if(current_string !== ''){
            $('.allsearch').trigger('click');
        }
    </script>

@endsection