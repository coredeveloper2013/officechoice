<header id="search-page">
    <div class="container">
        <div class="row">
            <!-- Logo -->
            <div class="col-md-2 col-sm-12 cols logo"><a href="{{ route('products')  }}"><img
                        src="{{asset('office/image/logo-s.png')}}"/></a></div>

            <!-- Search box -->
            <div class="col-sm-3 col-sm-12 cols search-item">
                <div id="search" class="input-group">

                    <input type="text" name="search" id="searchval" value="{{ $last_search  }}"
                           placeholder="Search your item here" class="form-control input-lg">
                    <span class="input-group-btn allsearch">
					<button type="button" class="btn btn-default btn-lg"><i class="ti-search "></i></button>
				</span>

                </div>
            </div>


            <!-- Supplier list -->
            <div class="col-md-2 col-sm-6 cols supplier-btn">
                <select id="supplier" class="form-control">
                    <option value="all">All Suppliers</option>

                    @foreach($suppliers as $supplier)
                        <option value="{{ $supplier->supplierCode }}">{{ $supplier->supplierName}}</option>
                    @endforeach


                </select>
            </div>

            <!-- Search options -->
            <div class="col-md-3 col-sm-6 cols supp-filter">
                <span class="btn-block center"> search options </span>
                <div class="col-sm-6 cols no-padding">
                    <span class="in-block"> <input name="allsupplier" type="checkbox" value="1"/> Multi View </span>
                    <span class="in-block"> <input name="oconly" type="checkbox" value="1"/> OC only </span>
                    <span class="in-block"> <input name="discontinued" type="checkbox" value="1"/> discontinued </span>
                </div>
                <div class="col-md-6 col-sm-6 cols no-padding">
                    <span class="in-block"> <input name="currentcatalog" type="checkbox" value="1"/> current catalogue </span>
                    <span class="in-block"> <input name="allwords" type="radio" value="1" checked="checked"/> all words </span>
                    <span class="in-block"> <input name="allwords" type="radio" value="0"/> exact phrase </span>
                </div>
            </div>
            <!-- Download icon -->
            <div class="col-md-1 col-sm-6 cols downloads" id='notification-link'>
                <a href="#" style="text-decoration:none;"> <span class="download-folder">
			download data
			 <i class="ti-folder"></i> </span></a></div>


            <div class="col-md-1 col-sm-6 cols downloads" id='notification-link'>
                <a href="{{route('custom_logout')}}" style="text-decoration:none;"> <span class="download-folder">
			Sign out  <br>
                    <i class="ti-power-off"></i></span>
                </a></div>

            <!-- <a href="logout.php">Logout</a> -->
        </div>
    </div>
</header>
