<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('guest')->name('login');

Route::post('/login','Auth\LoginController@Login')->name('custom_login');

Route::get('/log_me_out', function (){
    \Illuminate\Support\Facades\Auth::logout();
    return redirect('/');
})->name('custom_logout')->middleware('auth');



/* ----------- Forgot Password --------------*/
Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');



Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset/{token}', 'Auth\ResetPasswordController@reset')->name('password.update');


/*------------ Home -----------*/
Route::get('/products','ProductController@home')->middleware('auth')->name('products');
Route::get('/products/filter','ProductController@filter')->middleware('auth')->name('filter');
Route::get('/supplier/{suppliercode}/item/{supplieritem}','SupplierController@details')->middleware('auth')->name('supplier-item');


Route::get('/oclimport/{file}','ProductController@csv_download')->middleware('auth')->name('csv_download');

Route::get('/product/details/{code}','ProductController@details')->middleware('auth')->name('product.details');

Route::get('/invoice','homecontroller@showinvoice')->name('product.invoice');






