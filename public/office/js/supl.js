function printdiv(printdivname){
			var headstr = "<html><head><title>Print</title><style>#print-div{width: 21cm;min-height: 29.7cm;padding: 2cm;margin: 1cm auto;border: 1px #D3D3D3 solid;border-radius: 5px;background: white;box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);display:none;}   <link rel='stylesheet' href='./icons/themify-icons.css'><link rel='stylesheet' href='js/bootstrap/css/bootstrap.css'><link rel='stylesheet' href='css/styles.css'></style></head><body>";
			var footstr = "</body>";
			var newstr = document.getElementById(printdivname).innerHTML;
			var oldstr = document.body.innerHTML;
			document.body.innerHTML = headstr+newstr+footstr;
			window.print();
			document.body.innerHTML = oldstr;
			return false;
	}
	$(function () {
	 var nContainer = $(".notification-popup-container-main");

    //notification popup
    $("#notification-link").click(function () {
        $('.1').css('display','none');
        $('.2').css('display','none');
        $('.3').css('display','none');
        $('.4').css('display','none');
        $('.5').css('display','none');
        nContainer.fadeToggle(300);
        //$('.notification-popup-container-main').css('display','block');
        $('.has').css('display','block');
        return false;
    });

    //page click to hide the popup
    $(document).click(function () {
        nContainer.hide();
    });

    //popup notification bubble on click
    nContainer.click(function () {
        return false;
    });
});	 


 $(document).ready(function() {

     $('#key').change(function(){
         $('.1').css('display','none');
         $('.2').css('display','none');
         $('.3').css('display','none');
         $('.4').css('display','none');
         $('.5').css('display','none');
         var value = $('#key').val();
         console.log(value);
         if(value == 1){
             $(".1").slideDown();
         }
         else if(value == 2){
             $(".2").slideDown();
         }
         else if(value == 3){
             $(".3").slideDown();
         }
         else if(value == 4){
             $(".4").slideDown();
         }
         else if(value == 5){
             $(".5").slideDown();
         }

     })

	managedFileDownload();
});
$('#chkBrand').click(function(){
	managedFileDownload();
 });
$('#chkSiblings').click(function(){
	managedFileDownload();
 });
$('#chkFLC').click(function(){
	managedFileDownload();
 })
$('#chkEducation').click(function(){
	//alert("ok");
	managedFileDownloadEdu();
 });
$('#chkSort').click(function(){
	//alert("ok");
	managedFileDownloadSort();
 });
 $('#chkEducation1').click(function(){
	//alert("ok");
	managedFileDownloadEdu1();
 });
$('#chkSort1').click(function(){
	//alert("ok");
	managedFileDownloadSort1();
 });
$('#ddlSupplier').change(function(){
	var selectedval=$('#ddlSupplier').val();
	if($('#ddlSupplier').val()=="all"){
		$('.download3').empty();	
		$(".download3").append("<td style='padding:20px'> <a href='./oclimport/SPL2018.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");
		
	}
	else{
		$('.download3').empty();
	    var url="./oclimport/SPL2018"+selectedval+".csv";
		var checkurlval=checkUrl(url);
		//alert(checkurlval+url);
		if(checkurlval==true){
		$(".download3").append("<td style='padding:20px'> <a href='./oclimport/SPL2018"+selectedval+".csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}
		else{
			$(".download3").append("<td style='padding:20px;color:red;'> <strong>There is no price file for the supplier</strong> </td>");
		}
		//alert(selectedval);
	}
	
 });
$('#ddlFuture').change(function(){
	var selectedval=$('#ddlFuture').val();
	if($('#ddlFuture').val()=="all"){
		$('.download4').empty();	
		$(".download4").append("<td style='padding:20px'> <a href='./oclimport/FSPL2018.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");	
	}
	else{
		$('.download4').empty();
		 var url="./oclimport/FSPL2018"+selectedval+".csv";
		var checkurlval=checkUrl(url);
		//alert(checkurlval+url);
		if(checkurlval==true){
		$(".download4").append("<td style='padding:20px'> <a href='./oclimport/FSPL2018"+selectedval+".csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}
		else{
			$(".download4").append("<td style='padding:20px;color:red;'><strong> There is no price file for the supplier</strong> </td>");
		}
		//alert(selectedval);
	}
 });

function managedFileDownload()
	{
	
		if(document.getElementById("chkBrand").checked == true && document.getElementById("chkSiblings").checked==false && document.getElementById("chkFLC").checked==false){
		$('.download').empty();
		$(".download").append("<td style='padding:20px'> <a href='./oclimport/MANAGED_OC.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}	
		
		 
		if(document.getElementById("chkBrand").checked == true && document.getElementById("chkSiblings").checked==true && document.getElementById("chkFLC").checked==false){
		$('.download').empty();	
		$(".download").append("<td style='padding:20px'> <a href='./oclimport/MANAGED_OCSIB.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}	
		
		 
		if(document.getElementById("chkBrand").checked == true && document.getElementById("chkSiblings").checked==false && document.getElementById("chkFLC").checked==true){
		$('.download').empty();
		$(".download").append("<td style='padding:20px'> <a href='./oclimport/MANAGED_OC_FLC_PRINT.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}
		
		
		 if(document.getElementById("chkBrand").checked == true && document.getElementById("chkSiblings").checked==true && document.getElementById("chkFLC").checked==true){
		$('.download').empty();	
		$(".download").append("<td style='padding:20px'> <a href='./oclimport/MANAGED_OCSIB_FLC_PRINT.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}	
		
	
		  
		
		if(document.getElementById("chkBrand").checked == false && document.getElementById("chkSiblings").checked==false && document.getElementById("chkFLC").checked==false){
		$('.download').empty();	
		$(".download").append("<td style='padding:20px'> <a href='./oclimport/MANAGED_UB.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}	
		
		  if(document.getElementById("chkBrand").checked == false && document.getElementById("chkSiblings").checked==true && document.getElementById("chkFLC").checked==true){
		$('.download').empty();	
		$(".download").append("<td style='padding:20px'> <a href='./oclimport/MANAGED_UBSIB_FLC_PRINT.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}	
		
		 
		if(document.getElementById("chkBrand").checked == false && document.getElementById("chkSiblings").checked==true && document.getElementById("chkFLC").checked==false){
		$('.download').empty();
		$(".download").append("<td style='padding:20px'> <a href='./oclimport/MANAGED_UBSIB.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}
		
		if(document.getElementById("chkBrand").checked == false && document.getElementById("chkSiblings").checked==false && document.getElementById("chkFLC").checked==true){
		$('.download').empty();	
		$(".download").append("<td style='padding:20px'> <a href='./oclimport/MANAGED_UB_FLC_PRINT.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}	

	}
	function managedFileDownloadEdu()
	{
	
		if(document.getElementById("chkEducation").checked == true){
		$("#chkEducation1").attr("checked", false);
		$('.download1').empty();
		$(".download1").append("<td style='padding:20px'> <a href='./oclimport/EDUCATION_OC.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");
		}
		else{
			$('.download1').empty();
		}
		}
	function managedFileDownloadEdu1()
	{
		
		if(document.getElementById("chkEducation1").checked == true){
		$("#chkEducation").attr("checked", false);
		$('.download1').empty();	
		$(".download1").append("<td style='padding:20px'> <a href='./oclimport/EDUCATION_UB.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}
			else{
			$('.download1').empty();
		}
	}
	function managedFileDownloadSort()
	{
	
		if(document.getElementById("chkSort").checked == true ){
		$("#chkSort1").attr("checked", false);
		$('.download2').empty();
		$(".download2").append("<td style='padding:20px'> <a href='./oclimport/SORTFLCBAN.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");
		}	
		else{
			$('.download2').empty();	
		}
		}
	function managedFileDownloadSort1()
	{
		if(document.getElementById("chkSort1").checked == true ){
		$("#chkSort").attr("checked", false);
		
		$('.download2').empty();	
		$(".download2").append("<td style='padding:20px'> <a href='./oclimport/SORTFLCUNBAN.csv'><button type='button' class='btn btn-primary'>Download</button></a> </td>");}
			else{
			$('.download2').empty();	
		}
	}

	
function checkUrl(urlToFile)
{
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();

    if (xhr.status == "404" || xhr.getResponseHeader("Last-Modified") == null ) {
        console.log("File doesn't exist");
        return false;
    } else {
        console.log("File exists");
        return true;
    }
}  
function fun22(){
    $('.has').slideUp();
}





